package LinearAlgebra;
//Julian Mora
//1733574

public class Vector3d {
	private double x;
	private double y;
	private double z;
	
	public Vector3d(double newX, double newY, double newZ) {
		this.x = newX;
		this.y = newY;
		this.z = newZ;
	}
	
	public double getX() {
		return this.x;
	}
	
	public double getY() {
		return this.y;
	}
	
	public double getZ() {
		return this.z;
	}
	
	public double magnitude() {
		return Math.sqrt(Math.pow(this.x,2)+Math.pow(this.y,2)+Math.pow(this.z,2));
	}
	
	public double dotProduct(Vector3d a) {
		return (this.x*a.getX() + this.y*a.getY() + this.z*a.getZ()); 
	}
	
	public Vector3d add(Vector3d a) {
		double newX = this.x + a.getX();
		double newY = this.y + a.getY();
		double newZ = this.z + a.getZ();
		
		Vector3d newVector = new Vector3d(newX,newY,newZ);
		return newVector;
		
	}

}
