package LinearAlgebra;
//Julian Mora
//1733574
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testGetMethods() {
		Vector3d testVector = new Vector3d(1,2,3);
		assertEquals(1,testVector.getX());
		assertEquals(2,testVector.getY());
		assertEquals(3,testVector.getZ());
		}

	@Test
	void testMagnitude() {
		Vector3d testVector = new Vector3d(2,2,1);
		assertEquals(3,testVector.magnitude());
	}
	
	@Test
	void testMagnitudeSquared() {
		Vector3d testVector = new Vector3d(3,7,4);
		assertEquals(74, testVector.magnitude()*testVector.magnitude());
		
	}
	
	@Test 
	void testDotProduct() {
		Vector3d testVector1 = new Vector3d(3,7,4);
		Vector3d testVector2 = new Vector3d(5,2,1);
		
		assertEquals(33,testVector1.dotProduct(testVector2));
	}
	
	@Test
	void testXValueFromAddMethod() {
		Vector3d testVector1 = new Vector3d(3,7,4);
		Vector3d testVector2 = new Vector3d(5,2,1);
		
		assertEquals(8,testVector1.add(testVector2).getX());
	}
	
	@Test
	void testYValueFromAddMethod() {
		Vector3d testVector1 = new Vector3d(3,7,4);
		Vector3d testVector2 = new Vector3d(5,2,1);
		
		assertEquals(9,testVector1.add(testVector2).getY());
	}
	
	@Test
	void testZValueFromAddMethod() {
		Vector3d testVector1 = new Vector3d(3,7,4);
		Vector3d testVector2 = new Vector3d(5,2,1);
		
		assertEquals(5,testVector1.add(testVector2).getZ());
	}
}
